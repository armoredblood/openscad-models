use <threadlib/threadlib.scad>

height = 25;
width = 15;
threadWidth = 14;
thread = str("M",threadWidth,"-ext");
$fn=120;

module container(){
    difference(){
        cylinder(h=height, d=width);
        
        translate([0,0,2])
            cylinder(h=height-2, d=width-2);
        
        translate([0,0,height-8])
            thread(thread, turns=5);
        
        translate([0,0,height-2])
            cylinder(h=2,d1=threadWidth-2, d2=width); 
    }
}

module lid(){
    difference(){
        union(){
            translate([0,0,1])
                thread(thread, turns=5);
            
            cylinder(h=height-13, d=threadWidth-2);
                
            translate([0,0,height-13])
                cylinder(h=2,d1=threadWidth-2, d2=width);
            
            hull(){
                translate([0,0,height-11])
                    cylinder(h=3,d=width);
                
                translate([0,0,height-8])
                    sphere(d=width);
            }
        }
        
        hull(){
            translate([0,-7,height-5]) rotate([0,90,0])
                cylinder(h=width, d=10, center=true);
            
            translate([0,-7,height]) rotate([0,90,0])
                cylinder(h=width, d=10, center=true);
            
            translate([0,-14,height-5]) rotate([0,90,0])
                cylinder(h=width, d=10, center=true);
            
        }
        
        hull(){
            translate([0,7,height-5]) rotate([0,90,0])
                cylinder(h=width, d=10, center=true);
            
            translate([0,7,height]) rotate([0,90,0])
                cylinder(h=width, d=10, center=true);
            
            translate([0,14,height-5]) rotate([0,90,0])
                cylinder(h=width, d=10, center=true);
        }
        
        translate([0,0,height-5.5]) rotate([90,0,0])
                cylinder(h=width, d=5, center=true);
    }
}

lid();

translate([20,0,0])
    container();