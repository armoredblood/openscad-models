$fn=120; //comment this out for development to decrease render times

offsetRadius = .5;
circleRadius = 5;
profileHeight = 20;
profileDepth = 5;
holderLength = 64;
wallThickness = 1.5;
innerRadius = 13.5;
sideLength= (holderLength-(circleRadius*1.5)-innerRadius-wallThickness)/2;

// Create the basic shape of the 2d profile
module basicShape(){
    hull(){
        translate([circleRadius+offsetRadius,profileHeight/2,0])
            circle(circleRadius);
        translate([circleRadius+profileDepth,profileHeight-offsetRadius-circleRadius/2,0])
            square(circleRadius, center=true);
        translate([circleRadius+profileDepth,offsetRadius+circleRadius/2,0])
            square(circleRadius, center=true);
    }
}

// Create the 2d profile to rotate extrude later
module profile(){
    offset(r=offsetRadius){
        difference(){
            basicShape();
            translate([wallThickness-offsetRadius,0,0])
                basicShape();
        }
    }
}

module organizer(){
    difference(){
        union(){
            
            // Rounded Corners
            values = [[1,270], [-1,90]];
            for ( i = values ){
                translate([sideLength*i[0]/2,0,0]) rotate([0,0,i[1]])
                    rotate_extrude(angle=180,convexity=10)
                        translate([innerRadius,0,0])
                            profile();
            }
            
            // Straight Sides
            values2 = [[1,90], [-1,270]];
            for ( i = values2 ){
                translate([0,innerRadius*i[0],0]) rotate([90,0,i[1]])
                    linear_extrude(sideLength, center=true)
                        profile();
            }
        }
        
        // Cable Slots
        for (i = [0 : 90 : 90]){
            translate([0,0,profileHeight/2]) rotate([90,0,i])
                linear_extrude(height=holderLength*1.5, center=true)
                    hull(){
                        circle(2.3);
                        translate([0,profileHeight/2,0])
                            square(2,center=true);
                    }
        }
    }
}

organizer();