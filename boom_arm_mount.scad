mic_dia = 12.; //mm
mic_thickness = 8; //mm
height = 37; //mm

difference(){
    linear_extrude(height){
        difference(){
            union(){
                circle((mic_dia/2)+mic_thickness);
                translate([9,0,0])
                    square([15,27.8],true);
                translate([17,0,0])
                    square([10,70],true);
            }
        circle(mic_dia/2);
        }
    }
    union(){
        translate([10,-24,height*.75])
            rotate([0,90,0])
                cylinder(15,2,2);
        
        translate([10,-24,height*.25])
            rotate([0,90,0])
                cylinder(15,2,2);
        
        translate([10,24,height*.75])
            rotate([0,90,0])
                cylinder(15,2,2);
        
        translate([10,24,height*.25])
            rotate([0,90,0])
                cylinder(15,2,2);
    }
}